-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema metrocinemas
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema metrocinemas
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `metrocinemas` ;
USE `metrocinemas` ;

-- -----------------------------------------------------
-- Table `metrocinemas`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `metrocinemas`.`users` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(45) NULL,
  `last_name` VARCHAR(45) NULL,
  `password` VARCHAR(45) NULL,
  `user_type` INT NULL,
  `active` TINYINT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `metrocinemas`.`actors`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `metrocinemas`.`actors` (
  `id_actors` INT NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(45) NULL,
  `last_name` VARCHAR(45) NULL,
  PRIMARY KEY (`id_actors`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `metrocinemas`.`directors`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `metrocinemas`.`directors` (
  `id_directors` INT NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(45) NULL,
  `last_name` VARCHAR(45) NULL,
  PRIMARY KEY (`id_directors`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `metrocinemas`.`movies`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `metrocinemas`.`movies` (
  `id_movies` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(20) NULL,
  `country` VARCHAR(20) NULL,
  `year` VARCHAR(10) NULL,
  `clasification` VARCHAR(10) NULL,
  `duration` VARCHAR(10) NULL,
  `gender` VARCHAR(10) NULL,
  `sinopsys` VARCHAR(200) NULL,
  `id_actors` INT NOT NULL,
  `id_directors` INT NOT NULL,
  `url_trailer` VARCHAR(150) NULL,
  `url_cover` VARCHAR(150) NULL,
  `active` TINYINT NULL,
  PRIMARY KEY (`id_movies`),
  INDEX `fk_Movies_Actors_idx` (`id_actors` ASC),
  INDEX `fk_Movies_Directors1_idx` (`id_directors` ASC),
  CONSTRAINT `fk_Movies_Actors`
    FOREIGN KEY (`id_actors`)
    REFERENCES `metrocinemas`.`actors` (`id_actors`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Movies_Directors1`
    FOREIGN KEY (`id_directors`)
    REFERENCES `metrocinemas`.`directors` (`id_directors`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `metrocinemas`.`rooms`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `metrocinemas`.`rooms` (
  `id_rooms` INT NOT NULL AUTO_INCREMENT,
  `type` VARCHAR(45) NULL,
  `capacity` VARCHAR(45) NULL,
  `active` TINYINT NULL,
  PRIMARY KEY (`id_rooms`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `metrocinemas`.`functions`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `metrocinemas`.`functions` (
  `id_functions` INT NOT NULL AUTO_INCREMENT,
  `id_movies` INT NOT NULL,
  `id_rooms` INT NOT NULL,
  `schedule` VARCHAR(45) NULL,
  `start_date` VARCHAR(45) NULL,
  `finish_date` VARCHAR(45) NULL,
  `price` VARCHAR(45) NULL,
  `active` TINYINT NULL,
  PRIMARY KEY (`id_functions`),
  INDEX `fk_Functions_Movies1_idx` (`id_movies` ASC),
  INDEX `fk_Functions_Rooms1_idx` (`id_rooms` ASC),
  CONSTRAINT `fk_Functions_Movies1`
    FOREIGN KEY (`id_movies`)
    REFERENCES `metrocinemas`.`movies` (`id_movies`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Functions_Rooms1`
    FOREIGN KEY (`id_rooms`)
    REFERENCES `metrocinemas`.`rooms` (`id_rooms`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `metrocinemas`.`tickets`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `metrocinemas`.`tickets` (
  `id_tickets` INT NOT NULL AUTO_INCREMENT,
  `id_movies` INT NOT NULL,
  `id_rooms` INT NOT NULL,
  `id_functions` INT NOT NULL,
  `seats` VARCHAR(45) NULL,
  PRIMARY KEY (`id_tickets`),
  INDEX `fk_Tickets_Movies1_idx` (`id_movies` ASC),
  INDEX `fk_Tickets_Rooms1_idx` (`id_rooms` ASC),
  INDEX `fk_Tickets_Functions1_idx` (`id_functions` ASC),
  CONSTRAINT `fk_Tickets_Movies1`
    FOREIGN KEY (`id_movies`)
    REFERENCES `metrocinemas`.`movies` (`id_movies`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Tickets_Rooms1`
    FOREIGN KEY (`id_rooms`)
    REFERENCES `metrocinemas`.`rooms` (`id_rooms`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Tickets_Functions1`
    FOREIGN KEY (`id_functions`)
    REFERENCES `metrocinemas`.`functions` (`id_functions`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `metrocinemas`.`seats`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `metrocinemas`.`seats` (
  `id_seats` INT NOT NULL AUTO_INCREMENT,
  `seats` VARCHAR(45) NULL,
  `id_rooms` INT NOT NULL,
  `id_functions` INT NOT NULL,
  PRIMARY KEY (`id_seats`),
  INDEX `fk_Seats_Rooms1_idx` (`id_rooms` ASC),
  INDEX `fk_Seats_Functions1_idx` (`id_functions` ASC),
  CONSTRAINT `fk_Seats_Rooms1`
    FOREIGN KEY (`id_rooms`)
    REFERENCES `metrocinemas`.`rooms` (`id_rooms`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Seats_Functions1`
    FOREIGN KEY (`id_functions`)
    REFERENCES `metrocinemas`.`functions` (`id_functions`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `metrocinemas`.`tickets_detail`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `metrocinemas`.`tickets_detail` (
  `id_tickets_detail` INT NOT NULL AUTO_INCREMENT,
  `card_number` VARCHAR(45) NULL,
  `id_tickets` INT NOT NULL,
  `id_users` INT NOT NULL,
  `email` VARCHAR(45) NULL,
  `id_seats` INT NOT NULL,
  PRIMARY KEY (`id_tickets_detail`),
  INDEX `fk_TIcketsDetail_Tickets1_idx` (`id_tickets` ASC),
  INDEX `fk_TIcketsDetail_Users1_idx` (`id_users` ASC),
  INDEX `fk_TIcketsDetail_Seats1_idx` (`id_seats` ASC),
  CONSTRAINT `fk_TIcketsDetail_Tickets1`
    FOREIGN KEY (`id_tickets`)
    REFERENCES `metrocinemas`.`tickets` (`id_tickets`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_TIcketsDetail_Users1`
    FOREIGN KEY (`id_users`)
    REFERENCES `metrocinemas`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_TIcketsDetail_Seats1`
    FOREIGN KEY (`id_seats`)
    REFERENCES `metrocinemas`.`seats` (`id_seats`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `metrocinemas`.`emails`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `metrocinemas`.`emails` (
  `id_users` INT NOT NULL,
  `primary_email` VARCHAR(45) NULL,
  `secondary_email` VARCHAR(45) NULL,
  INDEX `fk_Emails_Users1_idx` (`id_users` ASC),
  CONSTRAINT `fk_Emails_Users1`
    FOREIGN KEY (`id_users`)
    REFERENCES `metrocinemas`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
