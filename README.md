# api

API of the metro cinema web application

The metrocinema pai is designed so that people can use it, either in any way you want, consult our functions to present them on your own site and promote us, or to make a suggestion of functions.

Currently it has only the development thought to be consumed by our branch and on our site.

But that does not take away the possibility that you can consume it, or set up your own instance.

Among the features that are currently available are:

- To be able to register, download, modify, or delete any movie.
- To be able to make a purchase of tickets, separating your place for some function.
- Consult all functions.
- be able to register as a user to have access to the purchase of tickets.

If you want to learn more about our api you can know more on our wiki:

[metrocinema Wiki](https://gitlab.com/metrocinema/api/wikis/home)