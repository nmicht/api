// FIXME El modelo no debe ejectuar queries ni diseñarlos, estos metodos deben existir en la clase abstracta de manejo de base de datos

const connectorDefault = require('./connector');

class Functions {
  constructor({ connector = connectorDefault, fields }) {
    this.fields = { ...fields };
    this.db = connector;
  }

  static createTable() {
    const sql = `CREATE TABLE IF NOT EXISTS metrocinemas.functions (
    id_functions INT NOT NULL AUTO_INCREMENT,
    id_movies INT NOT NULL,
    id_rooms INT NOT NULL,
    schedule VARCHAR(45) NULL,
    start_date VARCHAR(45) NULL,
    finish_date VARCHAR(45) NULL,
    price VARCHAR(45) NULL,
    active TINYINT NULL,
    PRIMARY KEY (id_functions),
    INDEX fk_Functions_Movies1_idx (id_movies ASC),
    INDEX fk_Functions_Rooms1_idx (id_rooms ASC),
    CONSTRAINT fk_Functions_Movies1
      FOREIGN KEY (id_movies)
      REFERENCES metrocinemas.movies (id_movies)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION,
    CONSTRAINT fk_Functions_Rooms1
      FOREIGN KEY (id_rooms)
      REFERENCES metrocinemas.rooms (id_rooms)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION)
  ENGINE = InnoDB;
  `;

    console.log('Preparing to create the functions table...');

    return new Promise((resolve, reject) => {
      connectorDefault.query(sql, error => {
        if (error) {
          return reject(error);
        }
        console.log('...functions table created!');
        return resolve('Success');
      });
    });
  }

  static insert({
    id_movies,
    id_rooms,
    schedule,
    start_date,
    finish_date,
    price,
    active
  }) {
    const sql = `INSERT INTO Functions
	    SET
		id_movies = (
			SELECT id_movies
            FROM movies
            WHERE name = ?
        ),
        id_rooms = (
			SELECT id_rooms
            FROM rooms
            WHERE type = ?
        ),
        schedule = ?,
        start_date = ?,
        finish_date = ?,
        price = ?,
        active = ?
        `;

    console.log(`Inserting function ${first_name} into database...`);

    return new Promise((resolve, reject) => {
      connectorDefault.query(
        sql,
        [id_movies, id_rooms, schedule, start_date, finish_date, price, active],
        (error, results, fields) => {
          if (error) {
            return reject(error);
          }
          console.log('====================================');
          console.log(results, fields);
          console.log('====================================');
          console.log(`...function ${results.insertId} inserted into database`);

          resolve(results);
        }
      );
    });
  }

  static find(id) {
    const baseSQL = 'SELECT * FROM functions';

    const sql = !id ? baseSQL : `${baseSQL} AND id = ? LIMIT 1`;

    console.log(`Querying for function id ${id}...`);

    return new Promise((resolve, reject) => {
      connectorDefault.query(sql, [id], (err, results, fields) => {
        if (err) {
          return reject(err);
        }
        console.log(`...found ${JSON.stringify(results)}!`);

        const functions = results.map(result => ({
          id_functions: result.id_functions,
          id_movies: result.id_movies,
          id_rooms: result.id_rooms,
          schedule: result.schedule,
          start_date: result.start_date,
          finish_date: result.finish_date,
          price: result.price,
          active: result.active
        }));

        if (!id) {
          return resolve(functions);
        }

        return resolve(functions[0]);
      });
    });
  }

  static deleteById(id) {
    const sql = 'DELETE FROM functions WHERE id_functions = ?';
    return new Promise((resolve, reject) => {
      if (!id) {
        return reject(new Error('Id is a neccesary field'));
      }
      connectorDefault.query(sql, [id], (error, results, fields) => {
        if (error) {
          return reject(error);
        }
        console.log('====================================');
        console.log(results, fields);
        console.log('====================================');

        resolve(results);
      });
    });
  }

  static updateById(id, fields = {}) {
    const keys = Object.keys(fields);
    let queryFields = keys
      .map(key => `${key} = ?, `)
      .reduce((total, field) => total + field);
    queryFields = queryFields.slice(0, queryFields.length - 2);
    const data = keys.map(key => fields[key]);

    console.log('====================================');
    console.log(queryFields, data);
    console.log('====================================');

    const sql = `UPDATE funcions SET ${queryFields} WHERE id_functions = ?`;
    return new Promise((resolve, reject) => {
      if (!id) {
        return reject(new Error('Id is a neccesary field'));
      }
      connectorDefault.query(sql, [...data, id], (error, results, fields) => {
        if (error) {
          return reject(error);
        }
        console.log('====================================');
        console.log(results, fields);
        console.log('====================================');

        resolve(results);
      });
    });
  }
}

module.exports = Functions;
