// FIXME El modelo no debe ejectuar queries ni diseñarlos, estos metodos deben existir en la clase abstracta de manejo de base de datos

const connectorDefault = require('./connector');

class Seats {
  constructor({ connector = connectorDefault, fields }) {
    this.fields = { ...fields };
    this.db = connector;
  }

  static createTable() {
    const sql = `metrocinemas.seats (
    id_seats INT NOT NULL AUTO_INCREMENT,
    seats VARCHAR(45) NULL,
    id_rooms INT NOT NULL,
    id_functions INT NOT NULL,
    PRIMARY KEY (id_seats),
    INDEX fk_Seats_Rooms1_idx (id_rooms ASC) VISIBLE,
    INDEX fk_Seats_Functions1_idx (id_functions ASC) VISIBLE,
    CONSTRAINT fk_Seats_Rooms1
        FOREIGN KEY (id_rooms)
        REFERENCES metrocinemas.rooms (id_rooms)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION,
    CONSTRAINT fk_Seats_Functions1
        FOREIGN KEY (id_functions)
        REFERENCES metrocinemas.functions (id_functions)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION)
    ENGINE = InnoDB;`;

    console.log('Preparing to create the seats table...');

    return new Promise((resolve, reject) => {
      connectorDefault.query(sql, error => {
        if (error) {
          return reject(error);
        }
        console.log('...seats table created!');
        return resolve('Success');
      });
    });
  }

  static insert({ seats, id_rooms, id_functions }) {
    const sql = `INSERT INTO seats
	    SET
		seats = ?,
        id_rooms = (
			SELECT id_rooms
            FROM rooms
            WHERE type = ?
        ),
        id_functions = (
			SELECT id_functions
            FROM functions
            WHERE schedule = ?
        )
        `;

    console.log(`Inserting seats ${seats} into database...`);

    return new Promise((resolve, reject) => {
      connectorDefault.query(
        sql,
        [seats, id_rooms, id_functions],
        (error, results, fields) => {
          if (error) {
            return reject(error);
          }
          console.log('====================================');
          console.log(results, fields);
          console.log('====================================');
          console.log(`...seat ${results.insertId} inserted into database`);

          resolve(results);
        }
      );
    });
  }

  static find(id) {
    const baseSQL = 'SELECT * FROM seats';

    const sql = !id ? baseSQL : `${baseSQL} AND id_seats = ? LIMIT 1`;

    console.log(`Querying for seat id ${id}...`);

    return new Promise((resolve, reject) => {
      connectorDefault.query(sql, [id], (err, results, fields) => {
        if (err) {
          return reject(err);
        }
        console.log(`...found ${JSON.stringify(results)}!`);

        const seats = results.map(result => ({
          id_seats: result.id_seats,
          seats: result.seats,
          id_rooms: result.id_rooms,
          id_functions: result.id_functions
        }));

        if (!id) {
          return resolve(seats);
        }

        return resolve(seats[0]);
      });
    });
  }

  static deleteById(id) {
    const sql = 'DELETE FROM seats WHERE id_seats = ?';
    return new Promise((resolve, reject) => {
      if (!id) {
        return reject(new Error('Id is a neccesary field'));
      }
      connectorDefault.query(sql, [id], (error, results, fields) => {
        if (error) {
          return reject(error);
        }
        console.log('====================================');
        console.log(results, fields);
        console.log('====================================');

        return resolve(results);
      });
    });
  }

  static updateById(id, fields = {}) {
    const keys = Object.keys(fields);
    let queryFields = keys
      .map(key => `${key} = ?, `)
      .reduce((total, field) => total + field);
    queryFields = queryFields.slice(0, queryFields.length - 2);
    const data = keys.map(key => fields[key]);

    console.log('====================================');
    console.log(queryFields, data);
    console.log('====================================');

    const sql = `UPDATE seats SET ${queryFields} WHERE id_seats = ?`;
    return new Promise((resolve, reject) => {
      if (!id) {
        return reject(new Error('Id is a neccesary field'));
      }
      connectorDefault.query(sql, [...data, id], (error, results, fields) => {
        if (error) {
          return reject(error);
        }
        console.log('====================================');
        console.log(results, fields);
        console.log('====================================');

        resolve(results);
      });
    });
  }
}

module.exports = Seats;
