// FIXME El modelo no debe ejectuar queries ni diseñarlos, estos metodos deben existir en la clase abstracta de manejo de base de datos

const connectorDefault = require('./connector');

class TicketsDetail {
  constructor({ connector = connectorDefault, fields }) {
    this.fields = { ...fields };
    this.db = connector;
  }

  static createTable() {
    const sql = `metrocinemas.tickets_detail (
    id_tickets_detail INT NOT NULL AUTO_INCREMENT,
    card_number VARCHAR(45) NULL,
    id_tickets INT NOT NULL,
    id_users INT NOT NULL,
    email VARCHAR(45) NULL,
    id_seats INT NOT NULL,
    PRIMARY KEY (id_tickets_detail),
    INDEX fk_TIcketsDetail_Tickets1_idx (id_tickets ASC) VISIBLE,
    INDEX fk_TIcketsDetail_Users1_idx (id_users ASC) VISIBLE,
    INDEX fk_TIcketsDetail_Seats1_idx (id_seats ASC) VISIBLE,
    CONSTRAINT fk_TIcketsDetail_Tickets1
        FOREIGN KEY (id_tickets)
        REFERENCES metrocinemas.tickets (id_tickets)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION,
    CONSTRAINT fk_TIcketsDetail_Users1
        FOREIGN KEY (id_users)
        REFERENCES metrocinemas.users (id)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION,
    CONSTRAINT fk_TIcketsDetail_Seats1
        FOREIGN KEY (id_seats)
        REFERENCES metrocinemas.seats (id_seats)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION)
    ENGINE = InnoDB;
`;

    console.log('Preparing to create the tickets detail table...');

    return new Promise((resolve, reject) => {
      connectorDefault.query(sql, error => {
        if (error) {
          return reject(error);
        }
        console.log('...tickets detail table created!');
        return resolve('Success');
      });
    });
  }

  static insert({ card_number, id_tickets, id_users, email, id_seats }) {
    const sql = `
    INSERT INTO TicketsDetail
    SET CardNumber = ?,
    id_tickets = (
      SELECT id_tickets
      FROM Tickets
      WHERE id_tickets = ?
    ),
    id_users = (
      SELECT id_users
      FROM Users
      WHERE id_users = ?
    ),
    Email = ?,
    id_seats = (
      SELECT id_seats
      FROM Seats
      WHERE Seats = ?
    )
    `;

    console.log(`Inserting tickets detail ${card_number} into database...`);

    return new Promise((resolve, reject) => {
      connectorDefault.query(
        sql,
        [card_number, id_tickets, id_users, email, id_seats],
        (error, results, fields) => {
          if (error) {
            return reject(error);
          }
          console.log('====================================');
          console.log(results, fields);
          console.log('====================================');
          console.log(
            `...ticket detail ${results.insertId} inserted into database`
          );

          resolve(results);
        }
      );
    });
  }

  static find(id) {
    const baseSQL = 'SELECT * FROM tickets_detail';

    const sql = !id ? baseSQL : `${baseSQL} AND id_tickets_detail = ? LIMIT 1`;

    console.log(`Querying for ticket id ${id}...`);

    return new Promise((resolve, reject) => {
      connectorDefault.query(sql, [id], (err, results, fields) => {
        if (err) {
          return reject(err);
        }
        console.log(`...found ${JSON.stringify(results)}!`);

        const tickets_detail = results.map(result => ({
          id_tickets_detail: result.id_tickets_detail,
          card_number: result.card_number,
          id_tickets: result.id_tickets,
          id_users: result.id_users,
          email: result.email,
          id_seats: result.id_seats
        }));

        if (!id) {
          return resolve(tickets_detail);
        }

        return resolve(tickets_detail[0]);
      });
    });
  }

  static deleteById(id) {
    const sql = 'DELETE FROM tickets_detail WHERE id_tickets_detail = ?';
    return new Promise((resolve, reject) => {
      if (!id) {
        return reject(new Error('Id is a neccesary field'));
      }
      connectorDefault.query(sql, [id], (error, results, fields) => {
        if (error) {
          return reject(error);
        }
        console.log('====================================');
        console.log(results, fields);
        console.log('====================================');

        resolve(results);
      });
    });
  }

  static updateById(id, fields = {}) {
    const keys = Object.keys(fields);
    let queryFields = keys
      .map(key => `${key} = ?, `)
      .reduce((total, field) => total + field);
    queryFields = queryFields.slice(0, queryFields.length - 2);
    const data = keys.map(key => fields[key]);

    console.log('====================================');
    console.log(queryFields, data);
    console.log('====================================');

    const sql = `UPDATE tickets_detail SET ${queryFields} WHERE id_tickets_detail = ?`;
    return new Promise((resolve, reject) => {
      if (!id) {
        return reject(new Error('Id is a neccesary field'));
      }
      connectorDefault.query(sql, [...data, id], (error, results, fields) => {
        if (error) {
          return reject(error);
        }
        console.log('====================================');
        console.log(results, fields);
        console.log('====================================');

        resolve(results);
      });
    });
  }
}

module.exports = TicketsDetail;
