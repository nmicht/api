// FIXME El modelo no debe ejectuar queries ni diseñarlos, estos metodos deben existir en la clase abstracta de manejo de base de datos

const connectorDefault = require('./connector');

class Rooms {
  constructor({ connector = connectorDefault, fields }) {
    this.fields = { ...fields };
    this.db = connector;
  }

  static createTable() {
    const sql = `metrocinemas.rooms (
    id_rooms INT NOT NULL AUTO_INCREMENT,
    type VARCHAR(45) NULL,
    capacity VARCHAR(45) NULL,
    active TINYINT NULL,
    PRIMARY KEY (id_rooms))
    ENGINE = InnoDB`;

    console.log('Preparing to create the rooms table...');

    return new Promise((resolve, reject) => {
      connectorDefault.query(sql, error => {
        if (error) {
          return reject(error);
        }
        console.log('...rooms table created!');
        return resolve('Success');
      });
    });
  }

  static insert({ type, capacity, active }) {
    const sql = `INSERT INTO rooms (
                type,
                capacity,
                active
                ) VALUES (?, ?, ?)`;

    console.log(`Inserting room ${type} into database...`);

    return new Promise((resolve, reject) => {
      connectorDefault.query(
        sql,
        [type, capacity, active || 1],
        (error, results, fields) => {
          if (error) {
            return reject(error);
          }
          console.log('====================================');
          console.log(results, fields);
          console.log('====================================');
          console.log(`...room ${results.insertId} inserted into database`);

          resolve(results);
        }
      );
    });
  }

  static find(id) {
    const baseSQL = 'SELECT * FROM rooms WHERE active = 1';

    const sql = !id ? baseSQL : `${baseSQL} AND id_rooms = ? LIMIT 1`;

    console.log(`Querying for room id ${id}...`);

    return new Promise((resolve, reject) => {
      connectorDefault.query(sql, [id], (err, results, fields) => {
        if (err) {
          return reject(err);
        }
        console.log(`...found ${JSON.stringify(results)}!`);

        const rooms = results.map(result => ({
          id_rooms: result.id_rooms,
          type: result.type,
          capacity: result.capacity,
          active: result.active
        }));

        if (!id) {
          return resolve(rooms);
        }

        return resolve(rooms[0]);
      });
    });
  }

  static deleteById(id) {
    const sql = 'UPDATE rooms SET active = 0 WHERE id = ?';
    return new Promise((resolve, reject) => {
      if (!id) {
        return reject(new Error('Id is a neccesary field'));
      }
      connectorDefault.query(sql, [id], (error, results, fields) => {
        if (error) {
          return reject(error);
        }
        console.log('====================================');
        console.log(results, fields);
        console.log('====================================');

        resolve(results);
      });
    });
  }

  static updateById(id, fields = {}) {
    const keys = Object.keys(fields);
    let queryFields = keys
      .map(key => `${key} = ?, `)
      .reduce((total, field) => total + field);
    queryFields = queryFields.slice(0, queryFields.length - 2);
    const data = keys.map(key => fields[key]);

    console.log('====================================');
    console.log(queryFields, data);
    console.log('====================================');

    const sql = `UPDATE rooms SET ${queryFields} WHERE id = ?`;
    return new Promise((resolve, reject) => {
      if (!id) {
        return reject(new Error('Id is a neccesary field'));
      }
      connectorDefault.query(sql, [...data, id], (error, results, fields) => {
        if (error) {
          return reject(error);
        }
        console.log('====================================');
        console.log(results, fields);
        console.log('====================================');

        resolve(results);
      });
    });
  }
}

module.exports = Rooms;
