// FIXME El modelo no debe ejectuar queries ni diseñarlos, estos metodos deben existir en la clase abstracta de manejo de base de datos

const connectorDefault = require('./connector');

class Movies {
  constructor({ connector = connectorDefault, fields }) {
    this.fields = { ...fields };
    this.db = connector;
  }

  static createTable() {
    const sql = `CREATE TABLE IF NOT EXISTS metrocinemas.movies (
    id_movies INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(45) NULL,
    country VARCHAR(45) NULL,
    year VARCHAR(45) NULL,
    clasification VARCHAR(45) NULL,
    duration VARCHAR(45) NULL,
    gender VARCHAR(45) NULL,
    sinopsys VARCHAR(45) NULL,
    id_actors INT NOT NULL,
    id_directors INT NOT NULL,
    url_trailer VARCHAR(150) NULL,
    url_cover VARCHAR(150) NULL,
    active TINYINT NULL,
    PRIMARY KEY (id_movies),
    INDEX fk_Movies_Actors_idx (id_actors ASC) VISIBLE,
    INDEX fk_Movies_Directors1_idx (id_directors ASC) VISIBLE,
    CONSTRAINT fk_Movies_Actors
      FOREIGN KEY (id_actors)
      REFERENCES metrocinemas.Actors (id_Actors)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION,
    CONSTRAINT fk_Movies_Directors1
      FOREIGN KEY (id_directors)
      REFERENCES metrocinemas.Directors (id_directors)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION)
  ENGINE = InnoDB;`;

    console.log('Preparing to create the movies table...');

    return new Promise((resolve, reject) => {
      connectorDefault.query(sql, error => {
        if (error) {
          return reject(error);
        }
        console.log('...movies table created!');
        return resolve('Success');
      });
    });
  }

  static insert({
    name,
    country,
    year,
    clasification,
    duration,
    gender,
    sinopsys,
    actors_idActors,
    directors_idDirectors,
    url_trailer,
    url_cover,
    active
  }) {
    const sql = `INSERT INTO movies (
                name,
                country,
                year,
                clasification,
                duration,
                gender,
                sinopsys,
                actors_idActors,
                directors_idDirectors,
                url_trailer,
                url_cover,
                active
                ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)`;

    console.log(`Inserting movie ${name} into database...`);

    return new Promise((resolve, reject) => {
      connectorDefault.query(
        sql,
        [
          name,
          country,
          year,
          clasification,
          duration,
          gender,
          sinopsys,
          actors_idActors,
          directors_idDirectors,
          url_trailer,
          url_cover,
          active
        ],
        (error, results, fields) => {
          if (error) {
            return reject(error);
          }
          console.log('====================================');
          console.log(results, fields);
          console.log('====================================');
          console.log(`...movie ${results.insertId} inserted into database`);

          resolve(results);
        }
      );
    });
  }

  static find(id) {
    const baseSQL = 'SELECT * FROM movies WHERE active = 1';

    const sql = !id ? baseSQL : `${baseSQL} AND id = ? LIMIT 1`;

    console.log(`Querying for movie id ${id}...`);

    return new Promise((resolve, reject) => {
      connectorDefault.query(sql, [id], (err, results, fields) => {
        if (err) {
          return reject(err);
        }
        console.log(`...found ${JSON.stringify(results)}!`);

        const movies = results.map(result => ({
          id: result.id,
          name: result.name,
          country: result.country,
          year: result.year,
          clasification: result.clasification,
          duration: result.duration,
          gender: result.gender,
          sinopsys: result.sinopsys,
          actors_idActors: result.actors_idActors,
          directors_idDirectors: result.directors_idDirectors,
          url_trailer: result.urlTrailer,
          url_cover: result.urlCover,
          active: result.active
        }));

        if (!id) {
          return resolve(movies);
        }

        return resolve(movies[0]);
      });
    });
  }

  static deleteById(id) {
    const sql = 'UPDATE movies SET active = 0 WHERE id = ?';
    return new Promise((resolve, reject) => {
      if (!id) {
        return reject(new Error('Id is a neccesary field'));
      }
      connectorDefault.query(sql, [id], (error, results, fields) => {
        if (error) {
          return reject(error);
        }
        console.log('====================================');
        console.log(results, fields);
        console.log('====================================');

        resolve(results);
      });
    });
  }

  static updateById(id, fields = {}) {
    const keys = Object.keys(fields);
    let queryFields = keys
      .map(key => `${key} = ?, `)
      .reduce((total, field) => total + field);
    queryFields = queryFields.slice(0, queryFields.length - 2);
    const data = keys.map(key => fields[key]);

    console.log('====================================');
    console.log(queryFields, data);
    console.log('====================================');

    const sql = `UPDATE Movies SET ${queryFields} WHERE id = ?`;
    return new Promise((resolve, reject) => {
      if (!id) {
        return reject(new Error('Id is a neccesary field'));
      }
      connectorDefault.query(sql, [...data, id], (error, results, fields) => {
        if (error) {
          return reject(error);
        }
        console.log('====================================');
        console.log(results, fields);
        console.log('====================================');

        resolve(results);
      });
    });
  }
}

module.exports = Movies;
