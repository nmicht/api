// FIXME El modelo no debe ejectuar queries ni diseñarlos, estos metodos deben existir en la clase abstracta de manejo de base de datos

const connectorDefault = require('./connector');

class Tickets {
  constructor({ connector = connectorDefault, fields }) {
    this.fields = { ...fields };
    this.db = connector;
  }

  static createTable() {
    const sql = `metrocinemas.tickets (
    id_tickets INT NOT NULL AUTO_INCREMENT,
    id_movies INT NOT NULL,
    id_rooms INT NOT NULL,
    id_functions INT NOT NULL,
    seats VARCHAR(45) NULL,
    PRIMARY KEY (id_tickets),
    INDEX fk_Tickets_Movies1_idx (id_movies ASC) VISIBLE,
    INDEX fk_Tickets_Rooms1_idx (id_rooms ASC) VISIBLE,
    INDEX fk_Tickets_Functions1_idx (id_functions ASC) VISIBLE,
    CONSTRAINT fk_Tickets_Movies1
        FOREIGN KEY (id_movies)
        REFERENCES metrocinemas.movies (id_movies)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION,
    CONSTRAINT fk_Tickets_Rooms1
        FOREIGN KEY (id_rooms)
        REFERENCES metrocinemas.rooms (id_rooms)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION,
    CONSTRAINT fk_Tickets_Functions1
        FOREIGN KEY (id_functions)
        REFERENCES metrocinemas.functions (id_functions)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION)
    ENGINE = InnoDB;`;

    console.log('Preparing to create the tickets table...');

    return new Promise((resolve, reject) => {
      connectorDefault.query(sql, error => {
        if (error) {
          return reject(error);
        }
        console.log('...tickets table created!');
        return resolve('Success');
      });
    });
  }

  static insert({ id_movies, id_rooms, id_functions, seats }) {
    const sql = `INSERT INTO tickets
	    SET
		id_movies = (
			SELECT id_movies
            FROM movies
            WHERE id_movies = ?
        ),
        id_rooms = (
			SELECT id_rooms
            FROM rooms
            WHERE id_rooms = ?
        ),
        id_functions = (
			SELECT id_functions
            FROM functions
            WHERE id_functions = ?
        ),
        Seats = ?`;

    console.log(`Inserting tickets ${seats} into database...`);

    return new Promise((resolve, reject) => {
      connectorDefault.query(
        sql,
        [id_movies, id_rooms, id_functions, seats],
        (error, results, fields) => {
          if (error) {
            return reject(error);
          }
          console.log('====================================');
          console.log(results, fields);
          console.log('====================================');
          console.log(`...ticket ${results.insertId} inserted into database`);

          resolve(results);
        }
      );
    });
  }

  static find(id) {
    const baseSQL = 'SELECT * FROM tickets';

    const sql = !id ? baseSQL : `${baseSQL} AND id_tickets = ? LIMIT 1`;

    console.log(`Querying for ticket id ${id}...`);

    return new Promise((resolve, reject) => {
      connectorDefault.query(sql, [id], (err, results, fields) => {
        if (err) {
          return reject(err);
        }
        console.log(`...found ${JSON.stringify(results)}!`);

        const tickets = results.map(result => ({
          id_tickets: result.id_tickets,
          id_movies: result.id_movies,
          id_rooms: result.id_rooms,
          id_functions: result.id_functions,
          seats: result.seats
        }));

        if (!id) {
          return resolve(tickets);
        }

        return resolve(tickets[0]);
      });
    });
  }

  static deleteById(id) {
    const sql = 'DELETE FROM tickets WHERE id_tickets = ?';
    return new Promise((resolve, reject) => {
      if (!id) {
        return reject(new Error('Id is a neccesary field'));
      }
      connectorDefault.query(sql, [id], (error, results, fields) => {
        if (error) {
          return reject(error);
        }
        console.log('====================================');
        console.log(results, fields);
        console.log('====================================');

        resolve(results);
      });
    });
  }

  static updateById(id, fields = {}) {
    const keys = Object.keys(fields);
    let queryFields = keys
      .map(key => `${key} = ?, `)
      .reduce((total, field) => total + field);
    queryFields = queryFields.slice(0, queryFields.length - 2);
    const data = keys.map(key => fields[key]);

    console.log('====================================');
    console.log(queryFields, data);
    console.log('====================================');

    const sql = `UPDATE tickets SET ${queryFields} WHERE id_tickets = ?`;
    return new Promise((resolve, reject) => {
      if (!id) {
        return reject(new Error('Id is a neccesary field'));
      }
      connectorDefault.query(sql, [...data, id], (error, results, fields) => {
        if (error) {
          return reject(error);
        }
        console.log('====================================');
        console.log(results, fields);
        console.log('====================================');

        resolve(results);
      });
    });
  }
}

module.exports = Tickets;
