// FIXME El modelo no debe ejectuar queries ni diseñarlos, estos metodos deben existir en la clase abstracta de manejo de base de datos

const connectorDefault = require('./connector');

class Emails {
  constructor({ connector = connectorDefault, fields }) {
    this.fields = { ...fields };
    this.db = connector;
  }

  static createTable() {
    const sql = `metrocinemas.emails (
    id_users INT NOT NULL,
    primary_email VARCHAR(45) NULL,
    secondary_email VARCHAR(45) NULL,
    INDEX fk_Emails_Users1_idx (id_users ASC) VISIBLE,
    CONSTRAINT fk_Emails_Users1
        FOREIGN KEY (id_users)
        REFERENCES metrocinemas.users (id_users)
        ON DELETE NO ACTION
        ON UPDATE NO `;

    console.log('Preparing to create the emails table...');

    return new Promise((resolve, reject) => {
      connectorDefault.query(sql, error => {
        if (error) {
          return reject(error);
        }
        console.log('...emails table created!');
        return resolve('Success');
      });
    });
  }

  static insert({ users_id_users, primary_email, email }) {
    const sql = `INSERT INTO emails (
                id_users,
                primary_email,
                email
                ) VALUES (?, ?)`;

    console.log(`Inserting email ${primary_email} into database...`);

    return new Promise((resolve, reject) => {
      connectorDefault.query(
        sql,
        [users_id_users, primary_email, email],
        (error, results, fields) => {
          if (error) {
            return reject(error);
          }
          console.log('====================================');
          console.log(results, fields);
          console.log('====================================');
          console.log(`...email ${results.insertId} inserted into database`);

          resolve(results);
        }
      );
    });
  }

  static find(id) {
    const baseSQL = 'SELECT * FROM emails';

    const sql = !id ? baseSQL : `${baseSQL} AND id = ? LIMIT 1`;

    console.log(`Querying for email id ${id}...`);

    return new Promise((resolve, reject) => {
      connectorDefault.query(sql, [id], (err, results, fields) => {
        if (err) {
          return reject(err);
        }
        console.log(`...found ${JSON.stringify(results)}!`);

        const emails = results.map(result => ({
          id_user: result.users_id_users,
          primary_email: result.primary_email,
          email: result.email
        }));

        if (!id) {
          return resolve(emails);
        }

        return resolve(emails[0]);
      });
    });
  }

  static deleteById(id) {
    const sql = 'DELETE FROM emails WHERE id_users = ?';
    return new Promise((resolve, reject) => {
      if (!id) {
        return reject(new Error('Id is a neccesary field'));
      }
      connectorDefault.query(sql, [id], (error, results, fields) => {
        if (error) {
          return reject(error);
        }
        console.log('====================================');
        console.log(results, fields);
        console.log('====================================');

        resolve(results);
      });
    });
  }

  static updateById(id, fields = {}) {
    const keys = Object.keys(fields);
    let queryFields = keys
      .map(key => `${key} = ?, `)
      .reduce((total, field) => total + field);
    queryFields = queryFields.slice(0, queryFields.length - 2);
    const data = keys.map(key => fields[key]);

    console.log('====================================');
    console.log(queryFields, data);
    console.log('====================================');

    const sql = `UPDATE actors SET ${queryFields} WHERE id = ?`;
    return new Promise((resolve, reject) => {
      if (!id) {
        return reject(new Error('Id is a neccesary field'));
      }
      connectorDefault.query(sql, [...data, id], (error, results, fields) => {
        if (error) {
          return reject(error);
        }
        console.log('====================================');
        console.log(results, fields);
        console.log('====================================');

        resolve(results);
      });
    });
  }
}

module.exports = Emails;
