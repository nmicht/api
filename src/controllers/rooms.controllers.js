/**
 * Retrieve all the rooms in the database
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 */
exports.getAllRooms = (req, res, next) => {
	Rooms.find()
    .then(rooms => {
      if (rooms.length === 0) {
        const err = new Error('There are no rooms in the database');
        err.statusCode = 404;
        return next(err);
      }
      return res.send(rooms);
    })
    .catch(next);
};

/**
 * Retrieve the ID from room in the database
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 */
exports.getOneRoom = (req, res, next) => {
	const {
    	params: { id_rooms }
  	} = req;

  	Rooms.find(id_rooms)
    	.then(room => {
      		if (!room.id) {
        		const err = new Error('There room does not exists in the database');
        		err.statusCode = 404;
        		return next(err);
      		}

      		return res.send(room);
    	})
    .catch(next);
};

/**
 * Create a new room and retrieve them back a response code
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 */
exports.postRoom = (req, res, next) => {
	const {
    	body: { type, capacity, active }
  	} = req;
  	Rooms.insert({
	    type,
	    capacity,
	    active: active ? active : 1
  	})
    	.then(({ insertId }) => {
      		if (!insertId) {
        		const err = new Error('could not be posible create room');
        		err.statusCode = 404;
        		return next(err);
      		}

      		return res.status(201).send({
		        id_rooms: insertId,
		        type,
			    capacity,
			    active
      		});
    	})
    .catch(next);
};

/**
 * Modify the complete room data that have the id given
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 */
exports.putRoom = (req, res, next) => {
	const {
	    body: { id_rooms, type, capacity, active }
	} = req;

	if (!id) {
	    const err = new Error('There are no an id provided to update');
	    err.statusCode = 422;
	    return next(err);
	}

	Rooms.updateById(id, {
	    id_rooms,
	    type,
	    capacity,
	    active
	})
	    .then(({ changedRows }) => {
	      	if (changedRows !== 0) {
	        	const err = new Error('There room data could not be updated');
	        	err.statusCode = 404;
	        	return next(err);
	      	}

	      	return res.send({
		        id_rooms,
		        type,
		        capacity,
		        active
	      	});
	    })
    .catch(next);
};

/**
 * Update the information of one room using it's id, with this route can modify only one field if neccessary
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 */
exports.patchRoom = (req, res, next) => {
	const { body } = req;
	const { id_rooms } = body;

	if (!id_rooms) {
	    const err = new Error('There are no an id provided to update');
	    err.statusCode = 422;
	    return next(err);
	}

	const fields = body;

	Rooms.updateById(id_rooms, fields)
	    .then(({ changedRows }) => {
	      	if (changedRows !== 0) {
	        	const err = new Error('There room data could not be updated');
	        	err.statusCode = 404;
	        	return next(err);
	      	}

	      	return res.send({
	        	...fields
	      	});
	    })
    .catch(next);
};

/**
 * Delete one movie according to his id, and then it its retrieved only to verify that the delete was exits
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 */
exports.deleteRoom = (req, res, next) => {
	const {
	    body: { id_rooms }
	} = req;

	if (!id_rooms) {
	    const err = new Error('There are no an id provided to delete');
	    err.statusCode = 422;
	    return next(err);
	}

	Rooms.deleteById(id_rooms)
	    .then(room => {
	      	if (changedRows !== 0) {
	        	const err = new Error('There room data could not be updated');
	        	err.statusCode = 404;
	        	return next(err);
	      	}

	      	return res.send({
	        	message: 'ok'
	      	});
	    })
    .catch(next);
};
