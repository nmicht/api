const usersControllers = require('./users.controllers');
const functionsControllers = require('./functions.controllers');
const seatsControllers = require('./seats.controllers');
const TicketDetailControllers = require('./TicketDetail.controllers');
const directorsControllers = require('./directors.controllers');
const actorsControllers = require('./actors.controllers');
const moviesControllers = require('./movies.controllers');
const roomsControllers = require('./rooms.controllers');
const ticketsControllers = require('./tickets.controllers');

module.exports = {
  usersControllers,
  functionsControllers,
  seatsControllers,
  TicketDetailControllers,
  directorsControllers,
  actorsControllers,
  moviesControllers,
  roomsControllers,
  ticketsControllers
};
