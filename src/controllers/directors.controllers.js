const { Directors } = require('../models');

/**
 * Retrieve all the directors in the database
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 */
exports.getAllDirectors = (req, res, next) => {
  Directors.find()
    .then(directors => {
      if (directors.length === 0) {
        const err = new Error('There are no directors in the database');
        err.statusCode = 404;
        return next(err);
      }
      return res.send(directors);
    })
    .catch(next);
};

/**
 * Retrieve a director in the database
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 */
exports.getOneDirector = (req, res, next) => {
  const {
    params: { id }
  } = req;

  Directors.find(id)
    .then(director => {
      if (!director.id_directors) {
        const err = new Error('There director does not exists in the database');
        err.statusCode = 404;
        return next(err);
      }

      return res.send(director);
    })
    .catch(next);
};

/**
 * Create a new director and retrieve them back a response code
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 */
exports.postDirector = (req, res, next) => {
  const {
    body: { first_name, last_name }
  } = req;
  Directors.insert({
    first_name,
    last_name,
    active: active ? active : 1
  })
    .then(({ insertId }) => {
      if (!insertId) {
        const err = new Error('could not be posible create director');
        err.statusCode = 404;
        return next(err);
      }

      return res.status(201).send({
        id_directors: insertId,
        first_name,
        last_name,
        active
      });
    })
    .catch(next);
};

/**
 * Modify the complete director data that have the id given
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 */
exports.putDirector = (req, res, next) => {
  const {
    body: { first_name, last_name, active },
    params: { id }
  } = req;

  if (!id) {
    const err = new Error('There are no an id provided to update');
    err.statusCode = 422;
    return next(err);
  }

  Directors.updateById(id, {
    id_directors: id,
    first_name,
    last_name,
    active
  })
    .then(({ changedRows }) => {
      if (changedRows !== 0) {
        const err = new Error('There director data could not be updated');
        err.statusCode = 404;
        return next(err);
      }

      return res.send({
        id_directors: id,
        first_name,
        last_name,
        active
      });
    })
    .catch(next);
};

/**
 * Update the information of one director using it's id,
 * with this route can modify only one field if neccessary
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 */
exports.patchDirector = (req, res, next) => {
  const { body } = req;
  const { id } = req.params;

  if (!id) {
    const err = new Error('There are no an id provided to update');
    err.statusCode = 422;
    return next(err);
  }

  const fields = body;

  Directors.updateById(id, fields)
    .then(({ changedRows }) => {
      if (changedRows !== 0) {
        const err = new Error('There director data could not be updated');
        err.statusCode = 404;
        return next(err);
      }

      return res.send({
        ...fields
      });
    })
    .catch(next);
};

/**
 * Delete one director according to his id,
 * and then it its retrieved only to verify that the delete was exits
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 */
exports.deleteDirector = (req, res, next) => {
  const {
    body: { id }
  } = req;

  if (!id) {
    const err = new Error('There are no an id provided to delete');
    err.statusCode = 422;
    return next(err);
  }

  Directors.deleteById(id)
    .then(director => {
      if (changedRows !== 0) {
        const err = new Error('There user data could not be updated');
        err.statusCode = 404;
        return next(err);
      }

      return res.send({
        message: 'ok'
      });
    })
    .catch(next);
};
