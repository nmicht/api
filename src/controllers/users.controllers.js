const { Users } = require('../models');

/**
 * Retrieve all the users in the database
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 */
exports.getAllUsers = (req, res, next) => {
  Users.find()
    .then(users => {
      if (users.length === 0) {
        const err = new Error('There are no users in the database');
        err.statusCode = 404;
        return next(err);
      }
      return res.send(users);
    })
    .catch(next);
};

/**
 * Creates a user and retrieve them back in response to the creation with exit
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 */
exports.postUSer = (req, res, next) => {
  const {
    body: { first_name, last_name, email, password, user_type, active }
  } = req;
  Users.insert({
    first_name,
    last_name,
    email,
    password,
    user_type,
    active: active ? active : 1
  })
    .then(({ insertId }) => {
      if (!insertId) {
        const err = new Error('could not be posible create user');
        err.statusCode = 404;
        return next(err);
      }

      return res.status(201).send({
        id: insertId,
        first_name,
        last_name,
        email,
        password,
        user_type,
        active
      });
    })
    .catch(next);
};

/**
 * Ends the sessions of one user
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 */
exports.logoutUser = (req, res, next) => res.send('loged out with exit');

/**
 * Grants access to the user, allowing him to do more actions
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 */
exports.loginUser = (req, res, next) => res.send('user loged in');

/**
 * Retrieves only a user by it's id
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 */
exports.getUser = (req, res, next) => {
  const {
    params: { id }
  } = req;

  Users.find(id)
    .then(user => {
      if (!user.id) {
        const err = new Error('There user does not exists in the database');
        err.statusCode = 404;
        return next(err);
      }

      return res.send(user);
    })
    .catch(next);
};

/**
 * Modify the complete user's data that have the id given
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 */
exports.putUser = (req, res, next) => {
  const {
    body: { first_name, last_name, email, password, user_type, active }
  } = req;
  const {
    params: { id }
  } = req;

  if (!id) {
    const err = new Error('There are no an id provided to update');
    err.statusCode = 422;
    return next(err);
  }

  Users.updateById(id, {
    id,
    first_name,
    last_name,
    email,
    password,
    user_type,
    active
  })
    .then(({ changedRows }) => {
      if (changedRows === 0) {
        const err = new Error('There user data could not be updated');
        err.statusCode = 404;
        return next(err);
      }

      return res.send({
        id,
        first_name,
        last_name,
        email,
        password,
        user_type,
        active
      });
    })
    .catch(next);
};

/**
 * Update the information of one user using it's id,
 * with this route can modify only one field if neccessary
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 */
exports.patchUser = (req, res, next) => {
  const { body } = req;
  const {
    params: { id }
  } = req;

  if (!id) {
    const err = new Error('There are no an id provided to update');
    err.statusCode = 422;
    return next(err);
  }

  const fields = body;

  Users.updateById(id, fields)
    .then(({ changedRows }) => {
      if (changedRows === 0) {
        const err = new Error('There user data could not be updated');
        err.statusCode = 404;
        return next(err);
      }

      return res.send({
        ...fields
      });
    })
    .catch(next);
};

/**
 * Delete one user according to his id,
 * and then it its retrieved only to verify that the delete was exits
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 */
exports.deleteUser = (req, res, next) => {
  const {
    params: { id }
  } = req;

  if (!id) {
    const err = new Error('There are no an id provided to delete');
    err.statusCode = 422;
    return next(err);
  }

  Users.deleteById(id)
    .then(({ changedRows }) => {
      if (changedRows === 0) {
        const err = new Error('There user data could not be deleted');
        err.statusCode = 404;
        return next(err);
      }

      return res.send({
        message: 'ok'
      });
    })
    .catch(next);
};
