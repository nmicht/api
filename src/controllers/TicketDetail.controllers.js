const { TicketsDetail } = require('../models');

/**
 * Retrieve all the tickets detail in the database
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 */
exports.getAllTicketDetails = (req, res, next) => {
  TicketsDetail.find()
    .then(ticketsDetail => {
      if (ticketsDetail.length === 0) {
        const err = new Error('There are no ticket details in the database');
        err.statusCode = 404;
        return next(err);
      }
      return res.send(ticketsDetail);
    })
    .catch(next);
};

/**
 * Retrieve the ID from ticket detail in the database
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 */
exports.getOneTicketDetail = (req, res, next) => {
  const {
    params: { id }
  } = req;

  TicketsDetail.find(id)
    .then(ticketDetail => {
      if (!ticketDetail.id_tickets_detail) {
        const err = new Error(
          'There ticket details does not exists in the database'
        );
        err.statusCode = 404;
        return next(err);
      }

      return res.send(ticketDetail);
    })
    .catch(next);
};

/**
 * Create a new Ticket Detail and retrieve them back a response code
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 */
exports.postTicketDetail = (req, res, next) => {
  const {
    body: { card_number, id_tickets, id_users, email, id_seats }
  } = req;
  TicketsDetail.insert({
    card_number,
    id_tickets,
    id_users,
    email,
    id_seats
  })
    .then(({ insertId }) => {
      if (!insertId) {
        const err = new Error('could not be posible create ticket details');
        err.statusCode = 404;
        return next(err);
      }

      return res.status(201).send({
        id_tickets_detail: insertId,
        card_number,
        id_tickets,
        id_users,
        email,
        id_seats
      });
    })
    .catch(next);
};

/**
 * Modify the complete Ticket Detail data that have the id given
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 */
exports.putTicketDetail = (req, res, next) => {
  const {
    body: { card_number, id_tickets, id_users, email, id_seats },
    params: { id }
  } = req;

  if (!id) {
    const err = new Error('There are no an id provided to update');
    err.statusCode = 422;
    return next(err);
  }

  TicketsDetail.updateById(id, {
    id_tickets_detail: id,
    card_number,
    id_tickets,
    id_users,
    email,
    id_seats
  })
    .then(({ changedRows }) => {
      if (changedRows !== 0) {
        const err = new Error('There ticket details data could not be updated');
        err.statusCode = 404;
        return next(err);
      }

      return res.send({
        id_tickets_detail: id,
        card_number,
        id_tickets,
        id_users,
        email,
        id_seats
      });
    })
    .catch(next);
};

/**
 * Update the information of one ticket detail using it's id, with this route can modify only one field if neccessary
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 */
exports.patchTicketDetail = (req, res, next) => {
  const { body } = req;
  const { id } = req.params;

  if (!id) {
    const err = new Error('There are no an id provided to update');
    err.statusCode = 422;
    return next(err);
  }

  const fields = body;

  TicketsDetail.updateById(id, fields)
    .then(({ changedRows }) => {
      if (changedRows !== 0) {
        const err = new Error('There ticekt details data could not be updated');
        err.statusCode = 404;
        return next(err);
      }

      return res.send({
        ...fields
      });
    })
    .catch(next);
};

/**
 * Delete one ticket detail according to his id,
 and then it its retrieved only to verify that the delete was exits
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 */
exports.deleteTicketDetail = (req, res, next) => {
  const {
    params: { id }
  } = req;

  if (!id) {
    const err = new Error('There are no an id provided to delete');
    err.statusCode = 422;
    return next(err);
  }

  TicketsDetail.deleteById(id)
    .then(({ changedRows }) => {
      if (changedRows !== 0) {
        const err = new Error('There ticket details data could not be updated');
        err.statusCode = 404;
        return next(err);
      }

      return res.send({
        message: 'ok'
      });
    })
    .catch(next);
};
