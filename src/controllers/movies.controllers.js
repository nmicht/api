/**
 * Retrieve all the movies in the database
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 */
exports.getAllMovies = (req, res, next) => res.send('getAllMovies');

/**
 * Retrieve the ID from movie in the database
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 */
exports.getOneMovie = (req, res, next) => res.send(req.body);

/**
 * Create a new movie and retrieve them back a response code
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 */
exports.postMovie = (req, res, next) => res.send(req.body);

/**
 * Modify the complete movie data that have the id given
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 */
exports.putMovie = (req, res, next) => res.send(req.body);

/**
 * Update the information of one movie using it's id, with this route can modify only one field if neccessary
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 */
exports.patchMovie = (req, res, next) => res.send(req.body);

/**
 * Delete one movie according to his id, and then it its retrieved only to verify that the delete was exits
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 */
exports.deleteMovie = (req, res, next) => res.send(req.body);
