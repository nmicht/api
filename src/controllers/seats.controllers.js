const { Seats } = require('../models');

/**
 * Retrieve all the seats in the database
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 */
exports.getAllSeats = (req, res, next) => {
  Seats.find()
    .then(seats => {
      if (seats.length === 0) {
        const err = new Error('There are no seats in the database');
        err.statusCode = 404;
        return next(err);
      }
      return res.send(seats);
    })
    .catch(next);
};

/**
 * Retrieve a seat by it's id
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 */
exports.getOneSeat = (req, res, next) => {
  const {
    params: { id }
  } = req;

  Seats.find(id)
    .then(seat => {
      if (!seat.id_seats) {
        const err = new Error('There seat does not exists in the database');
        err.statusCode = 404;
        return next(err);
      }

      return res.send(seat);
    })
    .catch(next);
};

/**
 * Creates a seat and retrieve them back in response to the creation with exit
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 */
exports.postSeat = (req, res, next) => {
  const {
    body: { seats, id_rooms, id_functions }
  } = req;
  Seats.insert({
    seats,
    id_rooms,
    id_functions
  })
    .then(({ insertId }) => {
      if (!insertId) {
        const err = new Error('could not be posible create seat');
        err.statusCode = 404;
        return next(err);
      }

      return res.status(201).send({
        id_seats: insertId,
        seats,
        id_rooms,
        id_functions
      });
    })
    .catch(next);
};

/**
 * Modify the complete seat data that have the id given
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 */
exports.putSeat = (req, res, next) => {
  const {
    body: { seats, id_rooms, id_functions },
    params: { id }
  } = req;

  if (!id) {
    const err = new Error('There are no an id provided to update');
    err.statusCode = 422;
    return next(err);
  }

  Seats.updateById(id, {
    id_seats: id,
    seats,
    id_rooms,
    id_functions
  })
    .then(({ changedRows }) => {
      if (changedRows !== 0) {
        const err = new Error('There seat data could not be updated');
        err.statusCode = 404;
        return next(err);
      }

      return res.send({
        id_seats: id,
        seats,
        id_rooms,
        id_functions
      });
    })
    .catch(next);
};

/**
 * update the complete seat data that have the id given
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 */
exports.patchSeat = (req, res, next) => {
  const {
    body,
    params: { id }
  } = req;

  if (!id) {
    const err = new Error('There are no an id provided to update');
    err.statusCode = 422;
    return next(err);
  }

  const fields = body;

  Seats.updateById(id, fields)
    .then(({ changedRows }) => {
      if (changedRows !== 0) {
        const err = new Error('There seat data could not be updated');
        err.statusCode = 404;
        return next(err);
      }

      return res.send({
        ...fields,
        id_seats: id
      });
    })
    .catch(next);
};

/**
 * delete the complete seat data that have the id given
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 */
exports.deleteSeat = (req, res, next) => {
  const {
    params: { id }
  } = req;

  if (!id) {
    const err = new Error('There are no an id provided to delete');
    err.statusCode = 422;
    return next(err);
  }

  Seats.deleteById(id)
    .then(seat => {
      if (changedRows !== 0) {
        const err = new Error('There seat data could not be updated');
        err.statusCode = 404;
        return next(err);
      }

      return res.send({
        message: 'ok'
      });
    })
    .catch(next);
};
