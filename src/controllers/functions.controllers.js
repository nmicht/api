const { Functions } = require('../models');

/**
 * Retrieve all the functions in the database
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 */
exports.getAllFunctions = (req, res, next) => {
  Functions.find()
    .then(functions => {
      if (functions.length === 0) {
        const err = new Error('There are no functions in the database');
        err.statusCode = 404;
        return next(err);
      }
      return res.send(functions);
    })
    .catch(next);
};

/**
 * Creates a function and retrieve them back in response to the creation with exit
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 */
exports.postFunction = (req, res, next) => {
  const {
    body: {
      id_movies,
      id_rooms,
      schedule,
      start_date,
      finish_date,
      price,
      active
    }
  } = req;
  Functions.insert({
    id_movies,
    id_rooms,
    schedule,
    start_date,
    finish_date,
    price,
    active: active ? active : 1
  })
    .then(({ insertId }) => {
      if (!insertId) {
        const err = new Error('could not be posible create function');
        err.statusCode = 404;
        return next(err);
      }

      return res.status(201).send({
        id_movies,
        id_rooms,
        schedule,
        start_date,
        finish_date,
        price,
        active
      });
    })
    .catch(next);
};

/**
 * Retrieves only a function by it's id
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 */
exports.getOneFunction = (req, res, next) => {
  const {
    params: { id }
  } = req;

  Functions.find(id)
    .then(func => {
      if (!func.id_functions) {
        const err = new Error('There function does not exists in the database');
        err.statusCode = 404;
        return next(err);
      }

      return res.send(func);
    })
    .catch(next);
};

/**
 * Modify the complete function data that have the id given
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 */
exports.putFunction = (req, res, next) => {
  const {
    body: {
      id_movies,
      id_rooms,
      schedule,
      start_date,
      finish_date,
      price,
      active
    }
  } = req;

  const {
    params: { id }
  } = req;

  if (!id) {
    const err = new Error('There are no an id provided to update');
    err.statusCode = 422;
    return next(err);
  }

  Functions.updateById(id, {
    id_functions: id,
    id_movies,
    id_rooms,
    schedule,
    start_date,
    finish_date,
    price,
    active
  })
    .then(({ changedRows }) => {
      if (changedRows !== 0) {
        const err = new Error('There function data could not be updated');
        err.statusCode = 404;
        return next(err);
      }

      return res.send({
        id_functions: id,
        id_movies,
        id_rooms,
        schedule,
        start_date,
        finish_date,
        price,
        active
      });
    })
    .catch(next);
};

/**
 * Update the information of one function using it's id, with this route can modify only one field if neccessary
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 */
exports.patchFunction = (req, res, next) => {
  const {
    body,
    params: { id }
  } = req;

  if (!id) {
    const err = new Error('There are no an id provided to update');
    err.statusCode = 422;
    return next(err);
  }

  const fields = body;

  Functions.updateById(id, fields)
    .then(({ changedRows }) => {
      if (changedRows !== 0) {
        const err = new Error('There function data could not be updated');
        err.statusCode = 404;
        return next(err);
      }

      return res.send({
        ...fields
      });
    })
    .catch(next);
};

/**
 * Delete one function according to his id, and then it its retrieved only to verify that the delete was exits
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 */
exports.deleteFunction = (req, res, next) => {
  const {
    params: { id }
  } = req;

  if (!id) {
    const err = new Error('There are no an id provided to delete');
    err.statusCode = 422;
    return next(err);
  }

  Functions.deleteById(id)
    .then(({ changedRows }) => {
      if (changedRows !== 0) {
        const err = new Error('There function data could not be updated');
        err.statusCode = 404;
        return next(err);
      }

      return res.send({
        message: 'ok'
      });
    })
    .catch(next);
};
