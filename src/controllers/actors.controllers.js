const { Actors } = require('../models');

/**
 * Retrieve all the actors in the database
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 */
exports.getAllActors = (req, res, next) => {
  Actors.find()
    .then(actors => {
      if (actors.length === 0) {
        const err = new Error('There are no actors in the database');
        err.statusCode = 404;
        return next(err);
      }
      return res.send(actors);
    })
    .catch(next);
};

/**
 * Retrieve a actor in the database
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 */
exports.getOneActor = (req, res, next) => {
  const {
    params: { id }
  } = req;

  Actors.find(id)
    .then(actor => {
      if (!actor.id_actors) {
        const err = new Error('There actor does not exists in the database');
        err.statusCode = 404;
        return next(err);
      }

      return res.send(actor);
    })
    .catch(next);
};

/**
 * Create a new actor and retrieve them back a response code
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 */
exports.postActor = (req, res, next) => {
  const {
    body: { first_name, last_name }
  } = req;
  Actors.insert({
    first_name,
    last_name,
    active: active ? active : 1
  })
    .then(({ insertId }) => {
      if (!insertId) {
        const err = new Error('could not be posible create actor');
        err.statusCode = 404;
        return next(err);
      }

      return res.status(201).send({
        id_actors: insertId,
        first_name,
        last_name,
        active
      });
    })
    .catch(next);
};

/**
 * Modify the complete actor data that have the id given
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 */
exports.putActor = (req, res, next) => {
  const {
    body: { first_name, last_name, active },
    params: { id }
  } = req;

  if (!id) {
    const err = new Error('There are no an id provided to update');
    err.statusCode = 422;
    return next(err);
  }

  Actors.updateById(id, {
    id_actors: id,
    first_name,
    last_name,
    active
  })
    .then(({ changedRows }) => {
      if (changedRows !== 0) {
        const err = new Error('There actor data could not be updated');
        err.statusCode = 404;
        return next(err);
      }

      return res.send({
        id_actors: id,
        first_name,
        last_name,
        active
      });
    })
    .catch(next);
};

/**
 * Update the information of one actor using it's id,
 * with this route can modify only one field if neccessary
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 */
exports.patchActor = (req, res, next) => {
  const { body } = req;
  const { id } = req.params;

  if (!id) {
    const err = new Error('There are no an id provided to update');
    err.statusCode = 422;
    return next(err);
  }

  const fields = body;

  Actors.updateById(id, fields)
    .then(({ changedRows }) => {
      if (changedRows !== 0) {
        const err = new Error('There actor data could not be updated');
        err.statusCode = 404;
        return next(err);
      }

      return res.send({
        ...fields
      });
    })
    .catch(next);
};

/**
 * Delete one actor according to his id,
 * and then it its retrieved only to verify that the delete was exits
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 */
exports.deleteActor = (req, res, next) => {
  const {
    params: { id }
  } = req;

  if (!id) {
    const err = new Error('There are no an id provided to delete');
    err.statusCode = 422;
    return next(err);
  }

  Actors.deleteById(id)
    .then(actor => {
      if (changedRows !== 0) {
        const err = new Error('There user data could not be updated');
        err.statusCode = 404;
        return next(err);
      }

      return res.send({
        message: 'ok'
      });
    })
    .catch(next);
};
