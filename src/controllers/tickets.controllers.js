const { Tickets } = require('../models');

/**
 * Retrieve all the tickets in the database
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 */
exports.getAllTickets = (req, res, next) => {
  Tickets.find()
    .then(tickets => {
      if (tickets.lenght === 0) {
        const err = new Error('There are no tickets in the database');
        err.statusCode = 404;
        return next(err);
      }
      return res.send(tickets);
    })
    .catch(next);
};

/**
 * Retrieve the ID from room in the database
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 */
exports.getOneTicket = (req, res, next) => {
  const {
    params: { id }
  } = req;

  Tickets.find(id)
    .then(ticket => {
      if (!ticket.id) {
        const err = new Error('There user does not exist in the database');
        err.statusCode = 404;
        return next(err);
      }

      return res.send(ticket);
    })
    .catch(next);
};

/**
 * Create a new ticket and retrieve them back a response code
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 */
exports.postTicket = (req, res, next) => {
  const {
    body: { id_movies, id_rooms, id_functions, seats }
  } = req;
  Tickets.insert({
    id_movies,
    id_rooms,
    id_functions,
    seats
  })
    .then(({ insertId }) => {
      if (!insertId) {
        const err = new Error('could not be posible create ticket');
        err.statusCode = 404;
        return next(err);
      }

      return res.status(201).send({
        id_tickets: insertId,
        id_movies,
        id_rooms,
        id_functions,
        seats
      });
    })
    .catch(next);
};

/**
 * Modify the complete ticket data that have the id given
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 */
exports.putTicket = (req, res, next) => {
  const {
    body: { id_movies, id_rooms, id_functions, seats },
    params: { id }
  } = req;

  if (!id) {
    const err = new Error('There are no an id provided to update');
    err.statusCode = 422;
    return next(err);
  }

  Tickets.updateById(id, {
    id_tickets: id,
    id_movies,
    id_rooms,
    id_functions,
    seats
  })
    .then(({ changedRows }) => {
      if (changedRows !== 0) {
        const err = new Error('There ticket data could not be updated');
        err.statusCode = 404;
        return next(err);
      }

      return res.send({
        id_tickets: id,
        id_movies,
        id_rooms,
        id_functions,
        seats
      });
    })
    .catch(next);
};

/**
 * Update the information of one ticket using it's id, with this route can modify only one field if neccessary
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 */
exports.patchTicket = (req, res, next) => {
  const { body } = req;
  const { id } = req.params;

  if (!id) {
    const err = new Error('There are no an id provided to update');
    err.statusCode = 422;
    return next(err);
  }

  const fields = body;

  Tickets.updateById(id, fields)
    .then(({ changedRows }) => {
      if (changedRows !== 0) {
        const err = new Error('There ticket data could not be updated');
        err.statusCode = 404;
        return next(err);
      }

      return res.send({
        ...fields,
        id_tickets: id
      });
    })
    .catch(next);
};

/**
 * Delete one ticket according to his id, and then it its retrieved only to verify that the delete was exits
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 */
exports.deleteTicket = (req, res, next) => {
  const {
    params: { id }
  } = req;

  if (!id) {
    const err = new Error('There are no an id provided to delete');
    err.statusCode = 422;
    return next(err);
  }

  Tickets.deleteById(id)
    .then(({ changedRows }) => {
      if (changedRows !== 0) {
        const err = new Error('There ticket data could not be updated');
        err.statusCode = 404;
        return next(err);
      }

      return res.send({
        message: 'ok'
      });
    })
    .catch(next);
};
