/**
 * Validates it the room's body request contains the valid fields to do the operations
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 */
exports.validRoomBody = (req, res, next) => {
  const {
    body: { type, capacity, active }
  } = req;
  if (!type) {
    return res.status(422).send({
      statusCode: 422,
      name: 'Error',
      message: 'invalid type field',
      code: 'UNPROCESSABLE_ENTITY'
    });
  }

  if (!capacity) {
    return res.status(422).send({
      statusCode: 422,
      name: 'Error',
      message: 'invalid capacity field',
      code: 'UNPROCESSABLE_ENTITY'
    });
  }

  next();
};
