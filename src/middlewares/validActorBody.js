const { withoutNumbers } = require('../utils');

/**
 * Validates if the request's body contains the fields necessary to continue the operations
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 */
exports.validActorBody = (req, res, next) => {
  const {
    body: { first_name, last_name }
  } = req;

  if (!withoutNumbers(first_name)) {
    return res.status(422).send({
      statusCode: 422,
      name: 'Error',
      message: 'invalid first_name field',
      code: 'UNPROCESSABLE_ENTITY'
    });
  }

  if (!withoutNumbers(last_name)) {
    return res.status(422).send({
      statusCode: 422,
      name: 'Error',
      message: 'invalid last_name field',
      code: 'UNPROCESSABLE_ENTITY'
    });
  }

  return next();
};
