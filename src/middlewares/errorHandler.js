/**
 * Methdo triggered when an error ocurs in the program execution
 * first show the error message in the console,
 * late if there isn't a error code set,
 * it means that the error was internally from the server and it should be a 500 status code
 * in other way, the status code from the error and the error message are send in response.
 * @param {Error} err
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 */
exports.errorHandler = (err, req, res, next) => {
  console.error(err.message);
  if (!err.statusCode) err.statusCode = 500;
  res.status(err.statusCode).send(err.message);
};
