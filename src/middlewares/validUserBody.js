const { withoutNumbers, validEmail } = require('../utils');

/**
 * Check if the request's body have all the necessary attributes to continue the operation
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 */
exports.validUserBody = (req, res, next) => {
  const {
    body: { first_name, last_name, email, password, user_type, active }
  } = req;

  if (!withoutNumbers(first_name)) {
    return res.status(422).send({
      statusCode: 422,
      name: 'Error',
      message: 'invalid first_name field',
      code: 'UNPROCESSABLE_ENTITY'
    });
  }

  if (!withoutNumbers(last_name)) {
    return res.status(422).send({
      statusCode: 422,
      name: 'Error',
      message: 'invalid last_name field',
      code: 'UNPROCESSABLE_ENTITY'
    });
  }

  if (!validEmail(email)) {
    return res.status(422).send({
      statusCode: 422,
      name: 'Error',
      message: 'invalid email field',
      code: 'UNPROCESSABLE_ENTITY'
    });
  }

  if (!password) {
    return res.status(422).send({
      statusCode: 422,
      name: 'Error',
      message: 'invalid password field',
      code: 'UNPROCESSABLE_ENTITY'
    });
  }

  if (!user_type) {
    return res.status(422).send({
      statusCode: 422,
      name: 'Error',
      message: 'invalid user_type field',
      code: 'UNPROCESSABLE_ENTITY'
    });
  }

  return next();
};
