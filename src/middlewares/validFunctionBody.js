/**
 * Validates it the function's body request contains the valid fields to do the operations
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 */
exports.validFunctionBody = (req, res, next) => {
  const {
    body: { schedule, start_date, finish_date, price, functions_col, active }
  } = req;

  // FIXME se debe validar tmb el formato, no solo vacio

  if (!schedule) {
    return res.status(422).send({
      statusCode: 422,
      name: 'Error',
      message: 'invalid schedule field',
      code: 'UNPROCESSABLE_ENTITY'
    });
  }

  if (!start_date) {
    return res.status(422).send({
      statusCode: 422,
      name: 'Error',
      message: 'invalid start_date field',
      code: 'UNPROCESSABLE_ENTITY'
    });
  }

  if (!finish_date) {
    return res.status(422).send({
      statusCode: 422,
      name: 'Error',
      message: 'invalid finish_date field',
      code: 'UNPROCESSABLE_ENTITY'
    });
  }

  if (!price) {
    return res.status(422).send({
      statusCode: 422,
      name: 'Error',
      message: 'invalid price field',
      code: 'UNPROCESSABLE_ENTITY'
    });
  }

  if (!functions_col) {
    return res.status(422).send({
      statusCode: 422,
      name: 'Error',
      message: 'invalid functions_col field',
      code: 'UNPROCESSABLE_ENTITY'
    });
  }

  return next();
};
