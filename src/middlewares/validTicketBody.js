/**
 * Check if the request's body have all the necessary attributes to continue the operation
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 */
exports.validTicketDoby = (req, res, next) => {
  const {
    body: { seats }
  } = req;

  // FIXME deberia validar que sea un asiento valido, no nada mas que no este vacio
  if (!seats) {
    return res.status(422).send({
      statusCode: 422,
      name: 'Error',
      message: 'invalid seats field',
      code: 'UNPROCESSABLE_ENTITY'
    });
  }

  return next();
};
