/**
 * Validates if the request's body is empty, only in case that the method's request needs it:
 * 'POST' or 'PUT' or 'PATCH'
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 */
exports.bodyEmpty = (req, res, next) => {
  console.log('====================================');
  console.log(req.method);
  console.log('====================================');
  if (req.method === 'GET' || req.method === 'DELETE') {
    return next();
  }

  if (Object.keys(req.body).length > 0) {
    return next();
  }

  return res.status(422).send({
    statusCode: 422,
    name: 'Error',
    message: 'Unprocessable Entity',
    code: 'UNPROCESSABLE_ENTITY'
  });
};
