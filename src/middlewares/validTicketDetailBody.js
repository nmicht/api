/**
 * Check if the request's body have all the necessary attributes to continue the operation
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 */
exports.validUserBody = (req, res, next) => {
  const {
    body: { card_number, email }
  } = req;

  // FIXME deberia validar que sean datos valido, no nada mas que no este vacio

  if (!card_number) {
    return res.status(422).send({
      statusCode: 422,
      name: 'Error',
      message: 'invalid card_number field',
      code: 'UNPROCESSABLE_ENTITY'
    });
  }

  if (!email) {
    return res.status(422).send({
      statusCode: 422,
      name: 'Error',
      message: 'invalid email field',
      dcode: 'UNPROCESSABLE_ENTITY'
    });
  }

  return next();
};
