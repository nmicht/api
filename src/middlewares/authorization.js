/**
 * Validate if in the request headers are the token from the sessions to see if send information
 * o an error
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 */
exports.authorization = (req, res, next) => {
  if (!req.headers.token) {
    return res.status(401).send({
      statusCode: 401,
      name: 'Error',
      message: 'Authorization Required',
      code: 'AUTHORIZATION_REQUIRED'
    });
  }
  return next();
};
