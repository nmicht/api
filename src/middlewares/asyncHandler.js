/**
 * Transform the async methods that aren't supported by the express framework
 * allowing us to run the code trought the DRY rules
 * @param {Function} fn async function to run like a normal method
 */
exports.asyncHandler = fn => (req, res, next) =>
  Promise.resolve(fn(req, res, next)).catch(next);
