/**
 *
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 */
exports.validSeatBody = (req, res, next) => {
  const {
    body: { seats }
  } = req;

  // FIXME deberia validar que sea una siento valido, no nada mas que no este vacio
  if (!seats) {
    return res.status(422).send({
      statusCode: 422,
      name: 'Error',
      message: 'invalid seats field',
      code: 'UNPROCESSABLE_ENTITY'
    });
  }
  return next();
};
