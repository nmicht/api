const { validTime, withoutNumbers } = require('../utils');

/**
 * Validates it the movie's body request contains the valid fields to do the operations
 * @param {Request} req
 * @param {Response} res
 * @param {Function} next
 */
exports.validMovieBody = (req, res, next) => {
  const {
    body: {
      name,
      country,
      year,
      clasification,
      duration,
      gender,
      sinopsys,
      moviescol,
      urlTrailer,
      urlCover,
      active
    }
  } = req;

  if (!name) {
    return res.status(422).send({
      statusCode: 422,
      name: 'Error',
      message: 'invalid name field',
      code: 'UNPROCESSABLE_ENTITY'
    });
  }

  if (!country) {
    return res.status(422).send({
      statusCode: 422,
      name: 'Error',
      message: 'invalid country field',
      code: 'UNPROCESSABLE_ENTITY'
    });
  }

  const actualYear = new Date().getFullYear();
  if (actualYear !== year) {
    return res.status(422).send({
      statusCode: 422,
      name: 'Error',
      message: 'invalid year field',
      code: 'UNPROCESSABLE_ENTITY'
    });
  }

  if (!clasification) {
    return res.status(422).send({
      statusCode: 422,
      name: 'Error',
      message: 'invalid clasification field',
      code: 'UNPROCESSABLE_ENTITY'
    });
  }

  if (!validTime(duration)) {
    return res.status(422).send({
      statusCode: 422,
      name: 'Error',
      message: 'invalid duration field',
      code: 'UNPROCESSABLE_ENTITY'
    });
  }

  if (!gender) {
    return res.status(422).send({
      statusCode: 422,
      name: 'Error',
      message: 'invalid gender field',
      code: 'UNPROCESSABLE_ENTITY'
    });
  }

  if (!sinopsys) {
    return res.status(422).send({
      statusCode: 422,
      name: 'Error',
      message: 'invalid sinopsys field',
      code: 'UNPROCESSABLE_ENTITY'
    });
  }

  if (!moviescol) {
    return res.status(422).send({
      statusCode: 422,
      name: 'Error',
      message: 'invalid moviescol field',
      code: 'UNPROCESSABLE_ENTITY'
    });
  }

  if (!urlTrailer) {
    return res.status(422).send({
      statusCode: 422,
      name: 'Error',
      message: 'invalid urlTrailer field',
      code: 'UNPROCESSABLE_ENTITY'
    });
  }

  if (!urlCover) {
    return res.status(422).send({
      statusCode: 422,
      name: 'Error',
      message: 'invalid urlCover field',
      code: 'UNPROCESSABLE_ENTITY'
    });
  }

  return next();
};
