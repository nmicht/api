# CASE 1: INSERT a new ticket MUST pre-exist MOVIE, ROOM, FUNCTION
# Get the foreign key from movie name, type of room, function schedule

INSERT INTO Tickets
	SET
		Movies_idMovies = (
			SELECT idMovies
            FROM Movies
            WHERE Name = 'avengers'
        ),
        Rooms_idRooms = (
			SELECT idRooms
            FROM Rooms
            WHERE Type = 'IMAX'
        ),
        Functions_idFunctions = (
			SELECT idFunctions
            FROM Functions
            WHERE Schedule = 'monday and wednesday 5-7 pm'
        ),
        Seats = '1A'