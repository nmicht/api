UPDATE Tickets
	SET
		Movies_idMovies = (
			SELECT idMovies
            FROM Movies
            WHERE Name = 'avengers'
        ),
        Rooms_idRooms = (
			SELECT idRooms
            FROM Rooms
            WHERE Type = 'IMAX'
        ),
        Functions_idFunctions = (
			SELECT idFunctions
            FROM Functions
            WHERE Schedule = 'monday and wednesday 5-7 pm'
        ),
        Seats = '1B'
	WHERE idTickets = 1;