# CASE 1: INSERT a new movie MUST pre-exist DIRECTOR AND ACTOR
# get the foreign key with the director and actor name

INSERT INTO Movies 
	SET
		Name = 'Avengers Infinity War',
		Country = 'United States',
        Year = '2018',
        Clasification = 'B15',
        Duration = '180',
        Gender = 'Science Fiction Action',
        Sinopsys = 'Really Nice Movie',
        Actors_idActors = (
			SELECT idActors
            FROM Actors
            WHERE FirstName = 'Christian'
        ),
        Directors_idDirectors = (
			SELECT idDirectors
            FROM Directors
            WHERE FirstName = 'Guillermo'
        ),
        URLTrailer = 'https://www.youtube.com/watch?v=6ZfuNTqbHE8',
        URLCover = 'https://www.imdb.com/title/tt4154756/mediaviewer/rm4044245504',
        Active = true