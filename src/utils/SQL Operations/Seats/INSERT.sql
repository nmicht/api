# CASE 1: INSERT a new seats into a room MUST pre-exist ROOMS AND FUNCIONS
# Get foreign key throw Type of the room and Schedule function

INSERT INTO Seats
	SET
		Seats = '30',
        Rooms_idRooms = (
			SELECT idRooms
            FROM Rooms
            WHERE Type = 'IMAX'
        ),
        Functions_idFunctions = (
			SELECT idFunctions
            FROM Functions
            WHERE Schedule = 'monday and wednesday 5-7 pm'
        )