UPDATE Seats
	SET
		Seats = '20',
        Rooms_idRooms = (
			SELECT idRooms
            FROM Rooms
            WHERE Type = 'IMAX'
        ),
        Functions_idFunctions = (
			SELECT idFunctions
            FROM Functions
            WHERE Schedule = 'monday and wednesday 5-7 pm'
        )
	WHERE idSeats = 1;