const { Router } = require('express');
const { usersControllers } = require('../controllers');
const { validUserBody, bodyEmpty, authorization } = require('../middlewares');

const router = Router();

router
  .get('/', authorization, usersControllers.getAllUsers)
  .post('/', bodyEmpty, validUserBody, usersControllers.postUSer)
  .get('/logout', authorization, usersControllers.logoutUser)
  .post('/login', bodyEmpty, usersControllers.loginUser)
  // FIXME se debe validar el parametro
  .get('/:id', authorization, usersControllers.getUser)
  // FIXME se debe validar el parametro
  .put(
    '/:id',
    authorization,
    bodyEmpty,
    validUserBody,
    usersControllers.putUser
  )
  // FIXME se debe validar el parametro
  .patch('/:id', authorization, bodyEmpty, usersControllers.patchUser)
  // FIXME se debe validar el parametro
  .delete('/:id', authorization, usersControllers.deleteUser);

module.exports = router;
