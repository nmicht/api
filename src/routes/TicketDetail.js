const { Router } = require('express');
const { TicketDetailControllers } = require('../controllers');
const {
  validTicketDetailBody,
  bodyEmpty,
  authorization
} = require('../middlewares');

const router = Router();

router
  .get('/', authorization, TicketDetailControllers.getAllTicketDetails)
  // FIXME se debe validar el parametro
  .get('/:id', authorization, TicketDetailControllers.getOneTicketDetail)
  .post('/', bodyEmpty, TicketDetailControllers.postTicketDetail)
  // FIXME se debe validar el parametro
  // FIXME se debe validar el cuerpo del request
  .put('/:id', authorization, TicketDetailControllers.putTicketDetail)
  // FIXME se debe validar el parametro
  .patch(
    '/:id',
    authorization,
    bodyEmpty,
    TicketDetailControllers.patchTicketDetail
  )
  // FIXME se debe validar el parametro
  .delete('/:id', authorization, TicketDetailControllers.deleteTicketDetail);

module.exports = router;
