const { Router } = require('express');
const { moviesControllers } = require('../controllers');

const router = Router();

// FIXME Falta validar cuerpo del request y parametros

router
  .get('/', moviesControllers.getAllMovies)
  .get('/:id', moviesControllers.getOneMovie)
  .post('/', moviesControllers.postMovie)
  .put('/:id', moviesControllers.putMovie)
  .patch('/:id', moviesControllers.patchMovie)
  .delete('/:id', moviesControllers.deleteMovie);

module.exports = router;
