const { Router } = require('express');
const { roomsControllers } = require('../controllers');
const { validUserBody, bodyEmpty, authorization } = require('../middlewares');

const router = Router();

router
  .get('/', authorization, roomsControllers.getAllRooms)
  // FIXME se debe validar el parametro
  .get('/:id', authorization, roomsControllers.getOneRoom)
  // FIXME la validacion deberia ser de rooms y no de user
  .post('/', bodyEmpty, validUserBody, roomsControllers.postRoom)
  // FIXME se debe validar el parametro
    // FIXME la validacion deberia ser de rooms y no de user
  .put(
    '/:id',
    authorization,
    bodyEmpty,
    validUserBody,
    roomsControllers.putRoom
  )
  // FIXME se debe validar el parametro
  .patch('/:id', authorization, bodyEmpty, roomsControllers.patchRoom)
  // FIXME se debe validar el parametro
  .delete('/:id', authorization, roomsControllers.deleteRoom);

module.exports = router;
