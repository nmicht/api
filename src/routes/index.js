const { Router } = require('express');

const actorsRouter = require('./actors');
const seatsRouter = require('./seats');
const moviesRouter = require('./movies');
const directorsRouter = require('./directors');
const roomsRouter = require('./rooms');
const functionsRouter = require('./functions');
const ticketDetailsRouter = require('./TicketDetail');
const ticketsRouter = require('./tickets');
const usersRouter = require('./users');

const router = Router();

router.use('/actors', actorsRouter);
router.use('/seats', seatsRouter);
router.use('/movies', moviesRouter);
router.use('/directors', directorsRouter);
router.use('/rooms', roomsRouter);
router.use('/functions', functionsRouter);
router.use('/ticket-details', ticketDetailsRouter);
router.use('/tickets', ticketsRouter);
router.use('/users', usersRouter);

module.exports = router;
