const { Router } = require('express');
const { actorsControllers } = require('../controllers');
const { validActorBody, bodyEmpty, authorization } = require('../middlewares');

const router = Router();

router
  .get('/', authorization, actorsControllers.getAllActors)
  // FIXME se debe validar el parametro
  .get('/:id', authorization, actorsControllers.getOneActor)
  .post('/', bodyEmpty, validActorBody, actorsControllers.postActor)
  // FIXME se debe validar el parametro
  .put(
    '/:id',
    authorization,
    bodyEmpty,
    validActorBody,
    actorsControllers.putActor
  )
  // FIXME se debe validar el parametro
  .patch('/:id', authorization, bodyEmpty, actorsControllers.patchActor)
  // FIXME se debe validar el parametro
  .delete('/:id', authorization, actorsControllers.deleteActor);

module.exports = router;
