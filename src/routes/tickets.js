const { Router } = require('express');
const { ticketsControllers } = require('../controllers');
const { validTicketBody, bodyEmpty, authorization } = require('../middlewares');

const router = Router();

// FIXME se debe validar el cuerpo del request y los parametros

router
  .get('/', authorization, ticketsControllers.getAllTickets)
  .get('/:id', authorization, ticketsControllers.getOneTicket)
  .post('/', bodyEmpty, ticketsControllers.postTicket)
  .put('/:id', authorization, bodyEmpty, ticketsControllers.putTicket)
  .patch('/:id', authorization, bodyEmpty, ticketsControllers.patchTicket)
  .delete('/:id', authorization, ticketsControllers.deleteTicket);

module.exports = router;
