const { Router } = require('express');
const { functionsControllers } = require('../controllers');
const {
  validFunctionBody,
  bodyEmpty,
  authorization
} = require('../middlewares');

const router = Router();

router
  .get('/', authorization, functionsControllers.getAllFunctions)
  .post('/', bodyEmpty, validFunctionBody, functionsControllers.postFunction)
  // FIXME se debe validar el parametro
  .get('/:id', authorization, functionsControllers.getOneFunction)
  // FIXME se debe validar el parametro
  .put(
    '/:id',
    authorization,
    bodyEmpty,
    validFunctionBody,
    functionsControllers.putFunction
  )
  // FIXME se debe validar el parametro
  .patch('/:id', authorization, bodyEmpty, functionsControllers.patchFunction)
  // FIXME se debe validar el parametro
  .delete('/:id', authorization, functionsControllers.deleteFunction);

module.exports = router;
