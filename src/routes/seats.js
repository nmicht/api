const { Router } = require('express');
const { seatsControllers } = require('../controllers');
const { validSeatBody, bodyEmpty, authorization } = require('../middlewares');

const router = Router();

router
  .get('/', authorization, seatsControllers.getAllSeats)
  // FIXME se debe validar el parametro
  .get('/:id', authorization, seatsControllers.getOneSeat)
  .post('/', bodyEmpty, validSeatBody, seatsControllers.postSeat)
  // FIXME se debe validar el parametro
  .put(
    '/:id',
    authorization,
    bodyEmpty,
    validSeatBody,
    seatsControllers.putSeat
  )
  // FIXME se debe validar el parametro
  .patch('/:id', authorization, bodyEmpty, seatsControllers.patchSeat)
  // FIXME se debe validar el parametro
  .delete('/:id', authorization, seatsControllers.deleteSeat);

module.exports = router;
