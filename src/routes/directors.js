const { Router } = require('express');
const { directorsControllers } = require('../controllers');
const {
  validDirectorBody,
  bodyEmpty,
  authorization
} = require('../middlewares');

const router = Router();

router
  .get('/', authorization, directorsControllers.getAllDirectors)
  // FIXME se debe validar el parametro
  .get('/:id', authorization, directorsControllers.getOneDirector)
  .post('/', bodyEmpty, validDirectorBody, directorsControllers.postDirector)
  // FIXME se debe validar el parametro
  .put(
    '/:id',
    authorization,
    bodyEmpty,
    validDirectorBody,
    directorsControllers.putDirector
  )
  // FIXME se debe validar el parametro
  .patch('/:id', authorization, bodyEmpty, directorsControllers.patchDirector)
  // FIXME se debe validar el parametro
  .delete('/:id', authorization, directorsControllers.deleteDirector);

module.exports = router;
