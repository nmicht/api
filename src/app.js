const express = require('express');
const bodyParser = require('body-parser');
const dotenv = require('dotenv');
const path = require('path');
const apiRouter = require('./routes');
const { errorHandler } = require('./middlewares');

dotenv.load({
  path: path.resolve(process.cwd(), '.env')
});

const app = express();
const {
  env: { PORT }
} = process;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(errorHandler);
app.use('/api', apiRouter);

app.listen(PORT, () => console.log(`Server listen on port ${PORT}`));
