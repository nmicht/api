const { it, describe, before, after } = require('mocha');
const { expect, should, assert } = require('chai');
const supertest = require('supertest');
const express = require('express');
const bodyParser = require('body-parser');
const {
  asyncHandler,
  authorization,
  bodyEmpty,
  validUserBody,
  validActorBody,
  validDirectorBody
} = require('../src/middlewares');

describe('#Testing functionality of the middlewares', () => {
  let server;
  let request;
  let delay;
  before(() => {
    server = express();
    server.use(bodyParser.json());
    server.use(bodyParser.urlencoded({ extended: false }));

    request = supertest(server);

    delay = time => {
      return new Promise((resolve, reject) => {
        setTimeout(() => {
          resolve(Date());
        }, time);
      });
    };
  });

  describe('- Testing the async handler', () => {
    before(() => {
      server.get(
        '/',
        asyncHandler(async (req, res) => {
          const conectioTime = await delay(100);
          res.send(`${conectioTime} yolo`);
        })
      );
    });
    it('should works like a normal endpoints worflow', done => {
      request
        .get('/')
        .expect(200)
        .end((err, { text }) => {
          if (err) {
            done(err);
            return;
          }
          expect(text).to.be.a('string');
          done();
        });
    });
  });

  describe('- Testing the auth handler', () => {
    before(() => {
      server.get('/auth', authorization, (req, res, next) => {
        try {
          res.status(200).send(Date());
        } catch (error) {
          console.error('aaaa', error);
        }
      });
    });

    it('should reject the request sending a 401 error status', done => {
      request
        .get('/auth')
        .expect(401)
        .end((err, { text }) => {
          if (err) {
            done(err);
            return;
          }
          done();
        });
    });

    it('should pass to the endpoint and give a status 200', done => {
      request
        .get('/auth')
        .set('token', 'Hola')
        .expect(200)
        .end((err, { text }) => {
          if (err) {
            done(err);
            return;
          }
          expect(text).to.be.a('string');
          done();
        });
    });
  });

  describe('- Testing bodyEmpty Middleware', () => {
    before(() => {
      server.get('/get', bodyEmpty, (req, res) => res.send(Date()));
      server.post('/', bodyEmpty, (req, res) => res.status(201).send(req.body));
    });

    it('should send an 201', done => {
      request
        .post('/')
        .send({
          Hi: 'hola'
        })
        .expect(201)
        .end((err, { text }) => {
          if (err) {
            done(err);
            return;
          }
          expect(text).to.be.a('string');
          done();
        });
    });

    it('should send an 422 error', done => {
      request
        .post('/')
        .expect(422)
        .end((err, { text }) => {
          if (err) {
            done(err);
            return;
          }

          expect(JSON.parse(text).name).to.be.equal('Error');
          done();
        });
    });

    it('should send a 200 status', done => {
      request
        .get('/get')
        .expect(200)
        .end((err, { text }) => {
          if (err) {
            done(err);
            return;
          }
          expect(text).to.be.a('string');
          done();
        });
    });
  });

  describe('- Testing validUserBody middleware', () => {
    before(() => {
      server.post('/users', validUserBody, (req, res, next) =>
        res.send('creado usuario con exito')
      );
    });

    it('should send a 200 status because the body is correct', done => {
      request
        .post('/users')
        .send({
          first_name: 'Fulanito',
          last_name: 'Fulano',
          email: 'fulano@correo.com',
          password: '1234',
          user_type: 1,
          active: true
        })
        .expect(200)
        .end((err, { text }) => {
          if (err) {
            done(err);
            return;
          }
          expect(text).to.be.a('string');
          expect(text).to.contain('creado usuario con exito');
          done();
        });
    });

    it('should send a 422 status because the body field first_name is incorrect', done => {
      request
        .post('/users')
        .send({
          first_name: '',
          last_name: 'Fulano',
          email: 'fulano@correo.com',
          password: '1234',
          user_type: 1,
          active: true
        })
        .expect(422)
        .end((err, { text }) => {
          if (err) {
            done(err);
            return;
          }
          expect(text).to.be.a('string');
          expect(text).to.contain('invalid first_name field');
          done();
        });
    });

    it('should send a 422 status because the body field last_name is incorrect', done => {
      request
        .post('/users')
        .send({
          first_name: 'Fulanito',
          last_name: '',
          email: 'fulano@correo.com',
          password: '1234',
          user_type: 1,
          active: true
        })
        .expect(422)
        .end((err, { text }) => {
          if (err) {
            done(err);
            return;
          }
          expect(text).to.be.a('string');
          expect(text).to.contain('invalid last_name field');
          done();
        });
    });

    it('should send a 422 status because the body field email is incorrect', done => {
      request
        .post('/users')
        .send({
          first_name: 'Fulanito',
          last_name: 'Fulano',
          email: 'fulanocorreo.com',
          password: '1234',
          user_type: 1,
          active: true
        })
        .expect(422)
        .end((err, { text }) => {
          if (err) {
            done(err);
            return;
          }
          expect(text).to.be.a('string');
          expect(text).to.contain('invalid email field');
          done();
        });
    });

    it('should send a 422 status because the body field password is incorrect', done => {
      request
        .post('/users')
        .send({
          first_name: 'Fulanito',
          last_name: 'Fulano',
          email: 'fulano@correo.com',
          password: '',
          user_type: 1,
          active: true
        })
        .expect(422)
        .end((err, { text }) => {
          if (err) {
            done(err);
            return;
          }
          expect(text).to.be.a('string');
          expect(text).to.contain('invalid password field');
          done();
        });
    });

    it('should send a 422 status because the body fiel user_type is incorrect', done => {
      request
        .post('/users')
        .send({
          first_name: 'Fulanito',
          last_name: 'Fulano',
          email: 'fulano@correo.com',
          password: '1234',
          active: true
        })
        .expect(422)
        .end((err, { text }) => {
          if (err) {
            done(err);
            return;
          }
          expect(text).to.be.a('string');
          expect(text).to.contain('invalid user_type field');
          done();
        });
    });

    it('should send a 200 status inclusively if the body field active is not there', done => {
      request
        .post('/users')
        .send({
          first_name: 'Fulanito',
          last_name: 'Fulano',
          email: 'fulano@correo.com',
          password: '1234',
          user_type: 1
        })
        .expect(200)
        .end((err, { text }) => {
          if (err) {
            done(err);
            return;
          }
          expect(text).to.be.a('string');
          expect(text).to.not.contain('invalid');
          expect(text).to.contain('creado usuario con exito');
          done();
        });
    });
  });

  describe('- Testing validActorBody middleware', () => {
    before(() => {
      server.post('/actors', validActorBody, (req, res, next) =>
        res.send(Date())
      );
    });

    it('should send a 200 status because the body is correct', done => {
      request
        .post('/actors')
        .send({
          first_name: 'Fulanito',
          last_name: 'Fulano'
        })
        .expect(200)
        .end((err, { text }) => {
          if (err) {
            done(err);
            return;
          }
          expect(text).to.be.a('string');
          done();
        });
    });

    it('should send a 422 status because the body field first_name is incorrect', done => {
      request
        .post('/actors')
        .send({
          first_name: '',
          last_name: 'Fulano'
        })
        .expect(422)
        .end((err, { text }) => {
          if (err) {
            done(err);
            return;
          }
          expect(text).to.be.a('string');
          expect(text).to.contain('invalid first_name field');
          done();
        });
    });

    it('should send a 422 status because the body field last_name is incorrect', done => {
      request
        .post('/actors')
        .send({
          first_name: 'Fulanito',
          last_name: ''
        })
        .expect(422)
        .end((err, { text }) => {
          if (err) {
            done(err);
            return;
          }
          expect(text).to.be.a('string');
          expect(text).to.contain('invalid last_name field');
          done();
        });
    });
  });

  describe('- Testing validDirectorBody middleware', () => {
    before(() => {
      server.post('/directors', validDirectorBody, (req, res, next) =>
        res.send(Date())
      );
    });

    it('should send a 200 status because the body is correct', done => {
      request
        .post('/directors')
        .send({
          first_name: 'Fulanito',
          last_name: 'Fulano'
        })
        .expect(200)
        .end((err, { text }) => {
          if (err) {
            done(err);
            return;
          }
          expect(text).to.be.a('string');
          done();
        });
    });

    it('should send a 422 status because the body field first_name is incorrect', done => {
      request
        .post('/directors')
        .send({
          first_name: '',
          last_name: 'Fulano'
        })
        .expect(422)
        .end((err, { text }) => {
          if (err) {
            done(err);
            return;
          }
          expect(text).to.be.a('string');
          expect(text).to.contain('invalid first_name field');
          done();
        });
    });

    it('should send a 422 status because the body field last_name is incorrect', done => {
      request
        .post('/directors')
        .send({
          first_name: 'Fulanito',
          last_name: ''
        })
        .expect(422)
        .end((err, { text }) => {
          if (err) {
            done(err);
            return;
          }
          expect(text).to.be.a('string');
          expect(text).to.contain('invalid last_name field');
          done();
        });
    });
  });

  after(() => {
    delete server;
    delete request;
  });
});
