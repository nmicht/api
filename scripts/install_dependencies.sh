#!/bin/bash

# any future command that fails will exit the script
set -e

# Delete the old repo
rm -rf /home/ubuntu/metrocinema_api/

# clone the repo again
git clone https://gitlab.com/metrocinema/api.git metrocinema_api

# starting pm2 daemon
pm2 status

cd /home/ubuntu/metrocinema_api

#install npm packages
echo "Running npm install"
sudo npm install

#Restart the node server
pm2 start ecosystem.json --env production